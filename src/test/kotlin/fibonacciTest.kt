import org.junit.Test
import java.math.BigInteger

class fibonacciTest {

    val fibonacci = fibonacciGame()

    @Test
    fun pos0ShouldReturn0 (){

        assert(fibonacci.getPosition(0).equals(0.toBigInteger()))

    }

    @Test
    fun pos1ShouldReturn1 (){

        assert(fibonacci.getPosition(1).equals(1.toBigInteger()))

    }

    @Test
    fun pos2ShouldReturn1 (){

        assert(fibonacci.getPosition(2).equals(1.toBigInteger()))

    }

    @Test
    fun pos3ShouldReturn2 (){

        assert(fibonacci.getPosition(3).equals(2.toBigInteger()))

    }

    @Test
    fun pos4ShouldReturn3 (){

        assert(fibonacci.getPosition(4).equals(3.toBigInteger()))

    }

    @Test
    fun pos31ShouldReturn1346269 (){

        assert(fibonacci.getPosition(31).equals(1346269.toBigInteger()))

    }


    @Test
    fun pos150ShouldReturn9969216677189303386214405760200 (){

        assert(fibonacci.getPosition(150).equals(BigInteger("9969216677189303386214405760200")))

    }


}